(function( $ ){

	$.fn.multipleInput = function(options) {

		// Default options
		var defaults = {
			checkDuplicateValue: true,
			position: "top",
			placeholder: ""
		};

		// Merge options with defaults
		var oSettings = $.extend( {}, defaults, options );

		if (TEMPLATE_VERSION == 'BOOTSTRAP_4') {
			sDeleteIconHTML = '<a href="#" class="multiple-input-close" title="Remove"><i class="fas fa-times"></i></a>';
		} else {
			sDeleteIconHTML = '<a href="#" class="multiple-input-close" title="Remove"><span class="glyphicon glyphicon-remove"></span></a>';
		}

		return this.each(function() {
			// $oOriginalInput refers to the input HTML node
			var $oOriginalInput = $(this);
			var $sList = $('<ul class="multiple-input-ul" />'); // create html elements - list of item addresses as unordered list

			if ($(this).val() != '' && IsJsonString($(this).val())) {
				$.each(jQuery.parseJSON($(this).val()), function(index, val) {
					$sList.append($('<li class="multiple-input-item"><span class="item-name" data-item="' + val.toLowerCase() + '">' + val + '</span></li>')
					  .prepend($(sDeleteIconHTML)
						   .click(function(e) { $(this).parent().remove(); refreshItems(); e.preventDefault(); })
					  )
					);
				});
			}

			var $oNewInput = $('<input type="text" class="multiple-input-input text-left '+$oOriginalInput.attr("class")+'" placeholder="' + oSettings.placeholder +'"/>').on('keydown', function(e) { // input
				$(this).removeClass('multiple-input-error');

				var iKeyNum;
				if(window.event){ // IE
					iKeyNum = e.keyCode;
				} else if(e.which){ // Netscape/Firefox/Opera
					iKeyNum = e.which;
                }

				// Supported key press is tab, enter, space or comma, there is no support for semi-colon since the keyCode differs in various browsers
				if(iKeyNum == 9 || iKeyNum == 32 || iKeyNum == 188) {
					displayItem($(this), oSettings.checkDuplicateValue);
				} else if (iKeyNum == 13) {
					displayItem($(this), oSettings.checkDuplicateValue);
					// Prevents enter key default
					// This is to prevent the form from submitting with the submit button
					// when you press enter in the item textbox
					e.preventDefault();
				}

			}).on('blur', function(event){
				if ($(this).val() != '') { displayItem($(this), oSettings.checkDuplicateValue); }
			});

			var $oContainer = $('<div class="multiple-input-container" />').click(function() { $oNewInput.focus(); } ); // container div

			// insert elements into DOM
			if (oSettings.position.toLowerCase() === "top") {
				$oContainer.append($sList).append($oNewInput).insertAfter($(this));
			} else if (oSettings.position.toLowerCase() === "bottom") {
				$oContainer.append($oNewInput).append($sList).insertBefore($(this));
			}

			/*
			oNewInput is the text input device.
			Value of the input could be a long line of copy-pasted items, not just a single item.
			As such, the string is tokenized, with each token validated individually.

			If the duplicateItemCheck variable is set to true, scans for duplicate items, and invalidates input if found.
			Otherwise allows items to have duplicated values if false.
			*/
			function displayItem(oNewInput, duplicateItemCheck) {

				// Remove space, comma and semi-colon from beginning and end of string
				// Does not remove inside the string as the item will need to be tokenized using space, comma and semi-colon
				var aInputValues = oNewInput.val().trim().replace(/^,|,$/g , '').replace(/^;|;$/g , '');
				// Remove the double quote
				aInputValues = aInputValues.replace(/"/g,"");
				// Split the string into an array, with the space, comma, and semi-colon as the separator
				aInputValues = aInputValues.split(/[\s,;]+/);

				for	(var i = 0; i < aInputValues.length; i++) {
					// Check if the item is already added, only if duplicateItemCheck is set to true
					if ( duplicateItemCheck === true && $oOriginalInput.val().indexOf(aInputValues[i]) != -1 ) {
				        if (aInputValues[i] && aInputValues[i].length > 0) {
							new function () {
								var existingElement = $sList.find('.item-name[data-item=' + aInputValues[i].toLowerCase().replace('.', '\\.').replace('@', '\\@') + ']');
								existingElement.css('font-weight', 'bold');
								setTimeout(function() { existingElement.css('font-weight', ''); }, 1500);
							}(); // Use a IIFE function to create a new scope so existingElement won't be overriden
						}
					}
					else {
						$sList.append($('<li class="multiple-input-item"><span class="item-name" data-item="' + aInputValues[i].toLowerCase() + '">' + aInputValues[i] + '</span></li>')
							  .prepend($(sDeleteIconHTML)
								   .click(function(e) { $(this).parent().remove(); refreshItems(); e.preventDefault(); })
							  )
						);
					}
				}

				oNewInput.val('');
				refreshItems();
			}

			function refreshItems () {
				var aItems = new Array();
				var oContainer = $oOriginalInput.siblings('.multiple-input-container');
				oContainer.find('.multiple-input-item span.item-name').each(function() { aItems.push($(this).html()); });
				$oOriginalInput.val(JSON.stringify(aItems)).trigger('change');
			}

			function IsJsonString(str) {
				try {
					JSON.parse(str);
				} catch (e) {
					return false;
				}

				return true;
			}

			return $(this).hide();
		});
	};

})(jQuery);
