# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/).

### [1.1.0] - 2019-12-16

#### Added
- Ability to change the front end template so we can use Bootstrap 4

### [1.0.0] - 2019-05-20

#### Added
- Created package from new, added JS and LESS
