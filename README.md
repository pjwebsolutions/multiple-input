# Multiple Input

Enter multiple items in a single input field with jQuery.

This is derived from the email address only input at https://github.com/pierresh/multiple-emails.js
It will allow multiple itesm to be added to one single text input. It's then encoded as JSON when submitted to the server.

## Usage

Attach to any text input that you want to be able to add multiple items too

~~~~
$(selector).multipleInput({options});
~~~~

## Options

| Option              | Description                                       | Type   | Values        | Default |
|---------------------|---------------------------------------------------|--------|---------------|---------|
| position            | Location of the items when added                  | string | bottom \| top | top     |
| placeholder         | Inserted as a placeholder in the input field      | string |               |         |
| checkDuplicateValue | Set to true to stop duplicate entries being added | bool   | true \| false | true    |
